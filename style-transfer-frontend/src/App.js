import React from "react";
import {
  Box,
  Container,
  FormControl,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  Typography,
} from "@material-ui/core";
import "fontsource-roboto";
import { algorithms } from "./config";

export default function App() {
  const [algorithm, setAlgorithm] = React.useState(algorithms[0]);

  const onAlgorithmChange = (event) => {
    setAlgorithm(event.target.value);
  };

  return (
    <Container>
      <br />
      <Paper elevation={3}>
        <br />
        <Typography variant="h3" component="h3" align="center">
          Style Transfer
        </Typography>
        <br />
        <form>
          <Box display="flex" justifyContent="center">
            <FormControl variant="outlined">
              <InputLabel>Algorithm</InputLabel>
              <Select
                label="Algorithm"
                value={algorithm}
                onChange={onAlgorithmChange}
              >
                {algorithms.map((a) => (
                  <MenuItem key={a.id} value={a}>
                    {a.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Box>
          <br />
          <algorithm.menuComponent algorithm={algorithm} />
        </form>
        <br />
      </Paper>
    </Container>
  );
}
