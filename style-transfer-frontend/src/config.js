import NAOAS from "./NAOAS";
import PLFRTSTASR from "./PLFRTSTASR";
import USTVFT from "./USTVFT";

export const algorithms = Object.freeze([
  {
    id: 0,
    name: "Neural Algorithm Of Artistic Style (Gatys et al., 2015)",
    menuComponent: NAOAS,
  },
  {
    id: 1,
    name:
      "Perceptual Losses For Real-Time Style Transfer And Super-Resolution (Johnson et al., 2016)",
    menuComponent: PLFRTSTASR,
  },
  {
    id: 2,
    name: "Universal Style Transfer Via Feature Transforms (Li et al., 2017)",
    menuComponent: USTVFT,
  },
]);

export const apiEndpoints = Object.freeze({
  contentImages: "/content-images",
  styleImages: "/style-images",
  modelImages: "/model-images",
  naoas: "/algorithms/naoas",
  plfrtstasr: "/algorithms/plfrtstasr",
  ustvft: "/algorithms/ustvft",
});
