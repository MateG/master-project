export function apiUrl(path, query = null) {
  let url = "http://localhost:5000" + path;
  if (query != null) {
    url += "?" + query;
  }
  return url;
}

export function fileName(path) {
  return path.substring(path.lastIndexOf("/") + 1);
}

export function formatSliderLabel(base, exponent) {
  const actualValue = base ** exponent;
  if (actualValue < 10) {
    return actualValue.toFixed(2);
  } else if (actualValue < 100) {
    return actualValue.toFixed(1);
  } else if (actualValue < 1000) {
    return actualValue.toFixed(0);
  } else {
    return (actualValue / 1000.0).toFixed(1) + "K";
  }
}

export function fixInt(newValue, rangeParams, defaultValue) {
  return fixNumber(
    (text) => parseInt(text, 10),
    newValue,
    rangeParams,
    defaultValue
  );
}

export function fixFloat(newValue, rangeParams, defaultValue) {
  return fixNumber(
    (text) => parseFloat(text),
    newValue,
    rangeParams,
    defaultValue
  );
}

function fixNumber(parse, newValue, rangeParams, defaultValue) {
  newValue = parse(newValue);
  if (isNaN(newValue)) {
    return defaultValue;
  }
  if (newValue < rangeParams.min) {
    return rangeParams.min;
  }
  if (newValue > rangeParams.max) {
    return rangeParams.max;
  }
  return newValue;
}
