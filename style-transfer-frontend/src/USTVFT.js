import React from "react";
import axios from "axios";
import {
  Box,
  Checkbox,
  FormControlLabel,
  FormGroup,
  Grid,
  Slider,
  TextField,
  Typography,
} from "@material-ui/core";
import { apiEndpoints } from "./config";
import { apiUrl, fileName, fixInt } from "./util";
import ImagePicker from "./components/ImagePicker";
import IndeterminateCircularProgressToggle from "./components/IndeterminateCircularProgressToggle";

const defaultConfig = {
  alpha: 0.5,
  outputSize: 256,
};
const alphaRange = { min: 0.0, max: 1.0, step: 0.01 };
const outputSizeRange = { min: 64, max: 768, step: 8 };

export default function USTVFT() {
  const [alpha, setAlpha] = React.useState(defaultConfig.alpha);
  const [contentImage, setContentImage] = React.useState();
  const [luminanceOnly, setLuminanceOnly] = React.useState(false);
  const [outputImage, setOutputImage] = React.useState();
  const [outputSize, setOutputSize] = React.useState(defaultConfig.outputSize);
  const [running, setRunning] = React.useState(false);
  const [styleImage, setStyleImage] = React.useState();

  const onRun = () => {
    setRunning(true);
    const config = {
      alpha,
      content: fileName(contentImage),
      luminanceOnly,
      outputSize,
      style: fileName(styleImage),
    };
    const query = Object.keys(config)
      .map(
        (key) => encodeURIComponent(key) + "=" + encodeURIComponent(config[key])
      )
      .join("&");
    axios
      .get(apiUrl(apiEndpoints.ustvft, query))
      .then((response) => {
        setOutputImage(response.data);
      })
      .then(() => setRunning(false));
  };

  return (
    <>
      <Grid container>
        <Grid item xs>
          <Box display="flex" justifyContent="center">
            <Typography>Content image</Typography>
          </Box>
          <br />
          <ImagePicker
            apiEndpoint={apiEndpoints.contentImages}
            image={contentImage}
            onChange={setContentImage}
            uploadEnabled
          />
        </Grid>
        <Grid item xs>
          <Box display="flex" justifyContent="center">
            <Typography>Style image</Typography>
          </Box>
          <br />
          <ImagePicker
            apiEndpoint={apiEndpoints.styleImages}
            image={styleImage}
            onChange={setStyleImage}
            uploadEnabled
          />
        </Grid>
      </Grid>
      <br />
      <Box display="flex" justifyContent="center">
        <Typography>Style weight (&alpha;)</Typography>
      </Box>
      <br />
      <Grid container spacing={2}>
        <Grid item xs={4} align="right">
          <Typography>Content</Typography>
        </Grid>
        <Grid item xs={4}>
          <Slider
            valueLabelDisplay="auto"
            value={alpha}
            min={alphaRange.min}
            max={alphaRange.max}
            step={alphaRange.step}
            onChange={(event, newValue) => setAlpha(newValue)}
          />
        </Grid>
        <Grid item xs={4}>
          <Typography>Style</Typography>
        </Grid>
      </Grid>
      <br />
      <Box display="flex" justifyContent="center">
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={luminanceOnly}
                onChange={(e, newValue) => setLuminanceOnly(newValue)}
              />
            }
            label="Luminance-only transfer"
          />
        </FormGroup>
      </Box>
      <br />
      <Box display="flex" justifyContent="center">
        <TextField
          variant="outlined"
          label="Output size"
          type="number"
          inputProps={outputSizeRange}
          value={outputSize}
          onChange={(event) =>
            setOutputSize(
              fixInt(
                event.target.value,
                outputSizeRange,
                defaultConfig.outputSize
              )
            )
          }
        />
      </Box>
      <br />
      <Box display="flex" justifyContent="center">
        <IndeterminateCircularProgressToggle
          disabled={contentImage == null || styleImage == null}
          running={running}
          onRun={onRun}
          onCancel={() => setRunning(false)}
        />
      </Box>
      <br />
      <Box display="flex" justifyContent="center">
        {outputImage != null ? (
          <img
            alt="Loading..."
            src={apiUrl(outputImage)}
            style={{ maxWidth: "100%" }}
          />
        ) : (
          ""
        )}
      </Box>
    </>
  );
}
