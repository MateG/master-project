import React from "react";
import {
  Box,
  Checkbox,
  FormControlLabel,
  FormGroup,
  Grid,
  Slider,
  TextField,
  Typography,
} from "@material-ui/core";
import { apiEndpoints } from "./config";
import { apiUrl, fileName, fixInt, formatSliderLabel } from "./util";
import CircularProgressToggle from "./components/CircularProgressToggle";
import ImagePicker from "./components/ImagePicker";

const defaultConfig = {
  maxIterations: 100,
  outputSize: 256,
  styleToContentRatioExponent: 3,
};
const maxIterationsRange = { min: 10, max: 2000, step: 10 };
const outputSizeRange = { min: 64, max: 768, step: 8 };
const styleToContentRatioExponentRange = { min: -7, max: 25, step: 1 };
const styleToContentRatioBase = 1.4;

export default function NAOAS() {
  const [contentImage, setContentImage] = React.useState();
  const [luminanceOnly, setLuminanceOnly] = React.useState(false);
  const [maxIterations, setMaxIterations] = React.useState(
    defaultConfig.maxIterations
  );
  const [progress, setProgress] = React.useState(0);
  const [running, setRunning] = React.useState(false);
  const [styleImage, setStyleImage] = React.useState();
  const [
    styleToContentRatioExponent,
    setStyleToContentRatioExponent,
  ] = React.useState(defaultConfig.styleToContentRatioExponent);
  const [outputImage, setOutputImage] = React.useState();
  const [outputSize, setOutputSize] = React.useState(defaultConfig.outputSize);

  const onRun = () => {
    setRunning(true);
    const config = {
      content: fileName(contentImage),
      luminanceOnly,
      maxIterations,
      outputSize,
      style: fileName(styleImage),
      styleToContentRatio: (
        styleToContentRatioBase ** styleToContentRatioExponent
      ).toFixed(2),
    };
    const query = Object.keys(config)
      .map(
        (key) => encodeURIComponent(key) + "=" + encodeURIComponent(config[key])
      )
      .join("&");
    const eventSource = new EventSource(apiUrl(apiEndpoints.naoas, query));
    eventSource.onmessage = onEventSourceMessage;
    eventSource.onerror = (error) => {
      eventSource.close();
      setProgress(100);
      setTimeout(() => setProgress(0), 500);
      setTimeout(() => setRunning(false), 500);
    };
  };

  const onEventSourceMessage = (event) => {
    const data = JSON.parse(event.data);
    setProgress(data.percentage);
    setOutputImage(data.path);
  };

  return (
    <>
      <Grid container>
        <Grid item xs>
          <Box display="flex" justifyContent="center">
            <Typography>Content image</Typography>
          </Box>
          <br />
          <ImagePicker
            apiEndpoint={apiEndpoints.contentImages}
            image={contentImage}
            onChange={setContentImage}
            uploadEnabled
          />
        </Grid>
        <Grid item xs>
          <Box display="flex" justifyContent="center">
            <Typography>Style image</Typography>
          </Box>
          <br />
          <ImagePicker
            apiEndpoint={apiEndpoints.styleImages}
            image={styleImage}
            onChange={setStyleImage}
            uploadEnabled
          />
        </Grid>
      </Grid>
      <br />
      <Box display="flex" justifyContent="center">
        <Typography>Style to content loss ratio (&beta;/&alpha;)</Typography>
      </Box>
      <br />
      <Grid container spacing={2}>
        <Grid item xs={4} align="right">
          <Typography>Content</Typography>
        </Grid>
        <Grid item xs={4}>
          <Slider
            valueLabelDisplay="auto"
            valueLabelFormat={(value) =>
              formatSliderLabel(styleToContentRatioBase, value)
            }
            value={styleToContentRatioExponent}
            min={styleToContentRatioExponentRange.min}
            max={styleToContentRatioExponentRange.max}
            step={styleToContentRatioExponentRange.step}
            onChange={(event, newValue) =>
              setStyleToContentRatioExponent(newValue)
            }
          />
        </Grid>
        <Grid item xs={4}>
          <Typography>Style</Typography>
        </Grid>
      </Grid>
      <br />
      <Box display="flex" justifyContent="center">
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                checked={luminanceOnly}
                onChange={(e, newValue) => setLuminanceOnly(newValue)}
              />
            }
            label="Luminance-only transfer"
          />
        </FormGroup>
      </Box>
      <br />
      <Box display="flex" justifyContent="center">
        <TextField
          variant="outlined"
          label="Max iterations"
          type="number"
          inputProps={maxIterationsRange}
          value={maxIterations}
          onChange={(event) =>
            setMaxIterations(
              fixInt(
                event.target.value,
                maxIterationsRange,
                defaultConfig.maxIterations
              )
            )
          }
        />
      </Box>
      <br />
      <Box display="flex" justifyContent="center">
        <TextField
          variant="outlined"
          label="Output size"
          type="number"
          inputProps={outputSizeRange}
          value={outputSize}
          onChange={(event) =>
            setOutputSize(
              fixInt(
                event.target.value,
                outputSizeRange,
                defaultConfig.outputSize
              )
            )
          }
        />
      </Box>
      <br />
      <Box display="flex" justifyContent="center">
        <CircularProgressToggle
          disabled={contentImage == null || styleImage == null}
          value={progress}
          running={running}
          onRun={onRun}
          onCancel={() => setRunning(false)}
        />
      </Box>
      <br />
      <Box display="flex" justifyContent="center">
        {outputImage != null ? (
          <img
            alt="Loading..."
            src={apiUrl(outputImage)}
            style={{ maxWidth: "100%" }}
          />
        ) : (
          ""
        )}
      </Box>
    </>
  );
}
