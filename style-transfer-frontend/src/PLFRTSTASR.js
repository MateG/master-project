import React from "react";
import axios from "axios";
import { Box, Grid, TextField, Typography } from "@material-ui/core";
import { apiEndpoints } from "./config";
import { apiUrl, fileName, fixInt } from "./util";
import ImagePicker from "./components/ImagePicker";
import IndeterminateCircularProgressToggle from "./components/IndeterminateCircularProgressToggle";

const defaultConfig = {
  outputSize: 256,
};
const outputSizeRange = { min: 64, max: 768, step: 8 };

export default function PLFRTSTASR() {
  const [contentImage, setContentImage] = React.useState();
  const [outputImage, setOutputImage] = React.useState();
  const [outputSize, setOutputSize] = React.useState(defaultConfig.outputSize);
  const [running, setRunning] = React.useState(false);
  const [modelImage, setModelImage] = React.useState();

  const onRun = () => {
    setRunning(true);
    const config = {
      content: fileName(contentImage),
      model: fileName(modelImage),
      outputSize,
    };
    const query = Object.keys(config)
      .map(
        (key) => encodeURIComponent(key) + "=" + encodeURIComponent(config[key])
      )
      .join("&");
    axios
      .get(apiUrl(apiEndpoints.plfrtstasr, query))
      .then((response) => {
        setOutputImage(response.data);
      })
      .then(() => setRunning(false));
  };

  return (
    <>
      <Grid container>
        <Grid item xs>
          <Box display="flex" justifyContent="center">
            <Typography>Content image</Typography>
          </Box>
          <br />
          <ImagePicker
            apiEndpoint={apiEndpoints.contentImages}
            image={contentImage}
            onChange={setContentImage}
            uploadEnabled
          />
        </Grid>
        <Grid item xs>
          <Box display="flex" justifyContent="center">
            <Typography>Trained style</Typography>
          </Box>
          <br />
          <ImagePicker
            apiEndpoint={apiEndpoints.modelImages}
            image={modelImage}
            onChange={setModelImage}
          />
        </Grid>
      </Grid>
      <br />
      <Box display="flex" justifyContent="center">
        <TextField
          variant="outlined"
          label="Output size"
          type="number"
          inputProps={outputSizeRange}
          value={outputSize}
          onChange={(event) =>
            setOutputSize(
              fixInt(
                event.target.value,
                outputSizeRange,
                defaultConfig.outputSize
              )
            )
          }
        />
      </Box>
      <br />
      <Box display="flex" justifyContent="center">
        <IndeterminateCircularProgressToggle
          disabled={contentImage == null || modelImage == null}
          running={running}
          onRun={onRun}
          onCancel={() => setRunning(false)}
        />
      </Box>
      <br />
      <Box display="flex" justifyContent="center">
        {outputImage != null ? (
          <img
            alt="Loading..."
            src={apiUrl(outputImage)}
            style={{ maxWidth: "100%" }}
          />
        ) : (
          ""
        )}
      </Box>
    </>
  );
}
