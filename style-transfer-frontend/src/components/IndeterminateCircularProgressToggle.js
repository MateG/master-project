import { Box, CircularProgress, IconButton } from "@material-ui/core";
import Pause from "@material-ui/icons/Pause";
import PlayArrow from "@material-ui/icons/PlayArrow";

export default function IndeterminateCircularProgressToggle(props) {
  return (
    <Box position="relative" display="inline-flex">
      <CircularProgress
        variant={props.running ? "indeterminate" : "determinate"}
        size={50}
      />
      <Box
        top={0}
        left={0}
        bottom={0}
        right={0}
        position="absolute"
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        <IconButton
          disabled={props.disabled}
          onClick={props.running ? props.onCancel : props.onRun}
        >
          {props.running ? <Pause /> : <PlayArrow />}
        </IconButton>
      </Box>
    </Box>
  );
}
