import React from "react";
import axios from "axios";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  GridList,
  GridListTile,
  IconButton,
  makeStyles,
} from "@material-ui/core";
import Add from "@material-ui/icons/Add";
import AddPhotoAlternate from "@material-ui/icons/AddPhotoAlternate";
import { apiUrl } from "../util";

const useStyles = makeStyles({
  image: {
    cursor: "pointer",
    opacity: 0.8,
    "&:hover": {
      opacity: 1.0,
    },
  },
  addButton: {
    width: "100%",
    height: "100%",
    "&:hover": {
      backgroundColor: "rgba(0,0,0,0.1)",
    },
  },
});

ImagePicker.defaultProps = {
  uploadEnabled: false,
};

export default function ImagePicker(props) {
  const { apiEndpoint, image, onChange, uploadEnabled } = props;
  const [opened, setOpened] = React.useState(false);
  const [images, setImages] = React.useState([]);
  const classes = useStyles();

  const fetchImages = () => {
    axios.get(apiUrl(apiEndpoint)).then((response) => setImages(response.data));
  };

  const handleOpened = () => {
    setOpened(true);
    fetchImages();
  };

  const handleFileChosen = (event) => {
    let data = new FormData();
    const file = event.target.files[0];
    console.log(file);
    data.append("image", file, file.name);
    axios
      .post(apiUrl(apiEndpoint), data, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then((response) => {
        setImages([response.data, ...images]);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleImagePicked = (image) => {
    onChange(image);
    setOpened(false);
  };

  return (
    <>
      <Box display="flex" justifyContent="center">
        {image != null ? (
          <Button component="label" onClick={handleOpened}>
            <img
              alt="Loading..."
              src={apiUrl(image)}
              style={{ maxWidth: "100%", maxHeight: 500 }}
            />
          </Button>
        ) : (
          <IconButton onClick={handleOpened}>
            <AddPhotoAlternate />
          </IconButton>
        )}
      </Box>
      <Dialog open={opened} onClose={() => setOpened(false)} maxWidth="lg">
        <DialogTitle>
          Choose {uploadEnabled && "or upload "}an image
        </DialogTitle>
        <DialogContent>
          <GridList
            cellHeight={256}
            cols={images.length < 4 ? images.length + 1 : 4}
          >
            {uploadEnabled && (
              <GridListTile>
                <ImageUploader handleFileChosen={handleFileChosen} />
              </GridListTile>
            )}
            {images.map((image) => (
              <GridListTile
                key={image}
                onClick={() => handleImagePicked(image)}
              >
                <img
                  className={classes.image}
                  src={apiUrl(image)}
                  alt="Loading..."
                />
              </GridListTile>
            ))}
          </GridList>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpened(false)} color="primary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

function ImageUploader(props) {
  const classes = useStyles();

  return (
    <Button className={classes.addButton} component="label">
      <Add />
      <input
        type="file"
        accept="image/*"
        hidden
        onChange={props.handleFileChosen}
      />
    </Button>
  );
}
