from flask import Flask

from api import schedulers
from api.config import STATIC_FOLDER, STATIC_URL_PATH
from api.handlers import *


def allow_cross_origin(response):
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


if __name__ == '__main__':
    app = Flask(__name__, static_folder=STATIC_FOLDER, static_url_path=STATIC_URL_PATH)

    app.after_request(allow_cross_origin)
    app.route("/content-images", methods=["GET"])(list_content_images)
    app.route("/style-images", methods=["GET"])(list_style_images)
    app.route("/model-images", methods=["GET"])(list_model_images)
    app.route("/content-images", methods=["POST"])(upload_content_image)
    app.route("/style-images", methods=["POST"])(upload_style_image)
    app.route("/algorithms/naoas")(run_naoas)
    app.route("/algorithms/plfrtstasr")(run_plfrtstasr)
    app.route("/algorithms/ustvft")(run_ustvft)

    schedulers.start_background_scheduler()
    app.run(host="0.0.0.0", debug=False, threaded=True)
