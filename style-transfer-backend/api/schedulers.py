import os
import shutil
from time import time

from apscheduler.schedulers.background import BackgroundScheduler

from api import logging
from api.config import OUTPUTS_CLEAN_UP_SECONDS, OUTPUTS_PATH


def newest_file_timestamp(directory):
    return max((os.path.getctime(os.path.join(directory, f)) for f in os.listdir(directory)), default=time())


def clean_up_outputs():
    images_dir = OUTPUTS_PATH
    for run_id in os.listdir(images_dir):
        run_id_path = os.path.join(images_dir, run_id)

        if os.path.isdir(run_id_path):
            creation_time = newest_file_timestamp(run_id_path)
            if creation_time < time() - OUTPUTS_CLEAN_UP_SECONDS:
                logging.info("Removing directory {}".format(run_id_path))
                shutil.rmtree(run_id_path)
        else:
            creation_time = os.path.getctime(run_id_path)
            if creation_time < time() - OUTPUTS_CLEAN_UP_SECONDS:
                logging.info("Removing {}".format(run_id_path))
                os.remove(run_id_path)


def start_background_scheduler():
    scheduler = BackgroundScheduler()
    scheduler.add_job(clean_up_outputs, trigger="interval", seconds=OUTPUTS_CLEAN_UP_SECONDS)
    scheduler.start()
