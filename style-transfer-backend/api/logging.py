from datetime import datetime


def info(message):
    print("[INFO {}] {}".format(datetime.now(), message))
