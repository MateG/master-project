import json
import os
import random
from queue import Queue
from threading import Thread

import torch
import torchvision
from flask import Response, request, abort, jsonify

from api import logging
from api.config import INPUTS_CONTENT_PATH, INPUTS_STYLE_PATH, OUTPUTS_PATH, INPUTS_MODELS_PATH, MODELS_FOLDER, \
    PLFRTSTASR_MODEL_PATH
from api.util import list_dir
from style_transfer.algorithms.naoas import NeuralAlgorithmOfArtisticStyle
from style_transfer.algorithms.plfrtstasr import PerceptualLossesForRealTimeStyleTransferAndSuperResolution
from style_transfer.algorithms.ustvft import UniversalStyleTransferViaFeatureTransforms
from utils import imsave


def list_content_images():
    return list_images(INPUTS_CONTENT_PATH)


def list_style_images():
    return list_images(INPUTS_STYLE_PATH)


def list_model_images():
    return list_images(INPUTS_MODELS_PATH)


def list_images(path):
    return jsonify(["/{}".format(path) for path in list_dir(path)])


def upload_content_image():
    return upload_image(INPUTS_CONTENT_PATH)


def upload_style_image():
    return upload_image(INPUTS_STYLE_PATH)


def upload_image(path):
    image = request.files["image"]
    if "." not in image.filename:
        abort(400)
    extension = image.filename.rsplit('.', 1)[1].lower()
    if extension not in ["jpg", "jpeg", "png"]:
        abort(400)

    image_id = "{:016x}".format(random.randrange(16 ** 16))
    image_path = os.path.join(path, "{}.{}".format(image_id, extension))
    logging.info("Saving image {}".format(image_path))
    image.save(image_path)

    return "/{}".format(image_path)


def run_naoas():
    config = Config()
    try:
        config.content_path = os.path.join(INPUTS_CONTENT_PATH, request.args["content"])
        config.luminance_only = request.args["luminanceOnly"] == "true"
        config.max_iter = int(request.args["maxIterations"])
        config.output_size = int(request.args["outputSize"])
        config.style_path = os.path.join(INPUTS_STYLE_PATH, request.args["style"])
        config.style_to_content_ratio = float(request.args["styleToContentRatio"])
    except ValueError:
        abort(400)

    def event_stream():
        run_id = "{:08x}".format(random.randrange(16 ** 8))
        run_id_path = os.path.join(OUTPUTS_PATH, run_id)
        os.mkdir(run_id_path)

        def algorithm_thread(algorithm):
            while algorithm.n_iter < config.max_iter:
                algorithm.perform_step()

        queue = Queue()
        Thread(
            target=algorithm_thread,
            args=(NeuralAlgorithmOfArtisticStyle(config, lambda msg: queue.put(msg)),)
        ).start()

        while True:
            n_iter, out_image = queue.get()
            percentage = min(100.0 * n_iter / config.max_iter, 100.0)
            path = "{}/{}.png".format(run_id_path, n_iter)
            out_image.save(path)

            yield "data: {}\n\n".format(json.dumps({"percentage": percentage, "path": "/{}".format(path)}))
            if n_iter >= config.max_iter:
                break

    return Response(event_stream(), mimetype="text/event-stream")


def run_plfrtstasr():
    config = Config()
    try:
        config.content_path = os.path.join(INPUTS_CONTENT_PATH, request.args["content"])
        config.model_path = os.path.splitext(os.path.join(PLFRTSTASR_MODEL_PATH, request.args["model"]))[0] + ".pth"
        config.output_size = int(request.args["outputSize"])
    except ValueError:
        abort(400)

    run_id = "{:08x}".format(random.randrange(16 ** 8))
    run_id_path = os.path.join(OUTPUTS_PATH, run_id)

    algorithm = PerceptualLossesForRealTimeStyleTransferAndSuperResolution(config)
    out_image = algorithm.get_output()
    path = "{}.png".format(run_id_path)
    imsave(out_image, path)

    return "/{}".format(path)


def run_ustvft():
    config = Config()
    try:
        config.alpha = float(request.args["alpha"])
        config.content_path = os.path.join(INPUTS_CONTENT_PATH, request.args["content"])
        config.luminance_only = request.args["luminanceOnly"] == "true"
        config.style_path = os.path.join(INPUTS_STYLE_PATH, request.args["style"])
        config.style_size = 256
        config.output_size = int(request.args["outputSize"])
    except ValueError:
        abort(400)

    run_id = "{:08x}".format(random.randrange(16 ** 8))
    run_id_path = os.path.join(OUTPUTS_PATH, run_id)

    algorithm = UniversalStyleTransferViaFeatureTransforms(config)
    output_image = algorithm.get_output()
    path = "{}.png".format(run_id_path)
    torchvision.utils.save_image(output_image, path, nrow=1)

    return "/{}".format(path)


class Config(object):
    pass
