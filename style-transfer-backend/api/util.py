import operator
import os


def list_dir(directory):
    return [f for f, _ in sorted(
        [(os.path.join(directory, f), os.path.getctime(os.path.join(directory, f))) for f in os.listdir(directory)],
        key=operator.itemgetter(1),
        reverse=True
    )]
