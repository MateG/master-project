import os

STATIC_FOLDER = "static"
INPUTS_PATH = os.path.join(STATIC_FOLDER, "inputs")
INPUTS_CONTENT_PATH = os.path.join(INPUTS_PATH, "content")
INPUTS_STYLE_PATH = os.path.join(INPUTS_PATH, "style")
INPUTS_MODELS_PATH = os.path.join(INPUTS_PATH, "models")
OUTPUTS_PATH = os.path.join(STATIC_FOLDER, "outputs")

STATIC_URL_PATH = "/static"

OUTPUTS_CLEAN_UP_SECONDS = 30

MODELS_FOLDER = "models"
NAOAS_MODEL_PATH = os.path.join(MODELS_FOLDER, "naoas")
PLFRTSTASR_MODEL_PATH = os.path.join(MODELS_FOLDER, "plfrtstasr")
USTVFT_MODEL_PATH = os.path.join(MODELS_FOLDER, "ustvft")
