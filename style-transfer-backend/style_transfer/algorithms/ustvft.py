import numpy as np
import torch
from PIL import Image
from torchvision import transforms

from api.config import USTVFT_MODEL_PATH
from style_transfer.algorithms.common import StyleTransferAlgorithm
from style_transfer.models.auto_encoder import MultiLevelAE


class UniversalStyleTransferViaFeatureTransforms(StyleTransferAlgorithm):
    def __init__(self, config):
        super().__init__(config)

        self.model = MultiLevelAE(USTVFT_MODEL_PATH).to(self.device)

        self.content_image = Image.open(self.config.content_path).convert("RGB")
        self.style_image = Image.open(self.config.style_path).convert("RGB")

        # if self.config.luminance_only:
        #     self.style_image = match_luminance(self.style_image, self.content_image)

        self.content = preprocess_image(
            self.content_image,
            config.output_size
        ).to(self.device)
        self.style = preprocess_image(
            self.style_image,
            config.style_size
        ).to(self.device)

    def get_output(self):
        with torch.no_grad():
            output = self.model(self.content, self.style, self.config.alpha).detach()

            if self.config.luminance_only:
                output_ycbcr = transforms.Compose([
                    transforms.ToPILImage(mode="RGB"),
                    transforms.Lambda(lambda rgb: rgb.convert("YCbCr")),
                    transforms.ToTensor()
                ])(output.squeeze())

                content_ycbcr = transforms.Compose([
                    transforms.Resize(output.shape[2:]),
                    transforms.ToPILImage(mode="RGB"),
                    transforms.Lambda(lambda rgb: rgb.convert("YCbCr")),
                    transforms.ToTensor()
                ])(self.content.detach().squeeze())

                content_ycbcr[0] = output_ycbcr[0]

                return transforms.Compose([
                    transforms.ToPILImage("YCbCr"),
                    transforms.Lambda(lambda ycbcr: ycbcr.convert("RGB")),
                    transforms.ToTensor()
                ])(content_ycbcr)
            else:
                return output


def preprocess_image(image, size):
    return transforms.Compose([
        transforms.Resize(size),
        transforms.ToTensor()
    ])(image).unsqueeze(0)


def match_luminance(style_image, content_image):
    style_image = style_image.convert("YCbCr")
    content_image = content_image.convert("YCbCr")

    content_y = np.array(content_image.getchannel("Y")).astype(float)
    style_y = np.array(style_image.getchannel("Y")).astype(float)
    style_y = (content_y.std() / style_y.std()) * (style_y - style_y.mean()) + content_y.mean()

    content_cb = np.array(content_image.getchannel("Cb")).astype(float)
    style_cb = np.array(style_image.getchannel("Cb")).astype(float)
    style_cb = (content_cb.std() / style_cb.std()) * (style_cb - style_cb.mean()) + content_cb.mean()

    content_cr = np.array(content_image.getchannel("Cr")).astype(float)
    style_cr = np.array(style_image.getchannel("Cr")).astype(float)
    style_cr = (content_cr.std() / style_cr.std()) * (style_cr - style_cr.mean()) + content_cr.mean()

    channels = (
        # Image.fromarray(np.uint8(np.clip(style_y, 15.0, 240.0))),
        # Image.fromarray(np.uint8(np.clip(style_cb, 15.0, 240.0))),
        # Image.fromarray(np.uint8(np.clip(style_cr, 15.0, 240.0))),
        Image.fromarray(np.uint8(np.clip(style_y, 0.0, 255.0))),
        Image.fromarray(np.uint8(np.clip(style_cb, 0.0, 255.0))),
        Image.fromarray(np.uint8(np.clip(style_cr, 0.0, 255.0))),
        # style_image.getchannel("Cb"),
        # style_image.getchannel("Cr")
    )
    return Image.merge("YCbCr", channels).convert("RGB")
