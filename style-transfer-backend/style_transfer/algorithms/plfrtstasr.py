import torch
from PIL import Image
from torchvision import transforms

from style_transfer.algorithms.common import StyleTransferAlgorithm
from style_transfer.image_processing import imagenet_normalize
from style_transfer.models.transform_network import TransformNetwork


class PerceptualLossesForRealTimeStyleTransferAndSuperResolution(StyleTransferAlgorithm):
    def __init__(self, config):
        super().__init__(config)

        self.transform_network = TransformNetwork()
        self.transform_network.load_state_dict(torch.load(config.model_path))
        self.transform_network = self.transform_network.to(self.device)

        self.content = preprocess_image(
            Image.open(self.config.content_path).convert("RGB"),
            self.config.output_size
        ).to(self.device)

    def get_output(self):
        return self.transform_network(self.content).detach()


def preprocess_image(image, size):
    return transforms.Compose([
        transforms.Resize(size),
        transforms.ToTensor(),
        imagenet_normalize
    ])(image).unsqueeze(0)
