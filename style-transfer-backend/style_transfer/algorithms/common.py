import torch

from api import logging


class StyleTransferAlgorithm(object):
    def __init__(self, config):
        self.config = config
        self.device = "cuda" if torch.cuda.is_available() else "cpu"
        logging.info("Starting {} on device={}\nconfig={}".format(
            self.__class__.__name__,
            self.device,
            self.config.__dict__
        ))
