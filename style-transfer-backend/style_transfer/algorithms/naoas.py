import os

import torch
import torch.nn as nn
from PIL import Image
from torch import optim
from torch.autograd import Variable
from torchvision import transforms

from api import logging
from api.config import NAOAS_MODEL_PATH
from style_transfer.algorithms.common import StyleTransferAlgorithm
from style_transfer.image_processing import imagenet_denormalize, imagenet_normalize
from style_transfer.models.vgg import load_vgg


class GramMatrix(nn.Module):
    def forward(self, x):
        b, c, h, w = x.size()
        F = x.view(b, c, h * w)
        G = torch.bmm(F, F.transpose(1, 2))
        G.div_(h * w)
        return G


class GramMSELoss(nn.Module):
    def forward(self, x, target):
        return nn.MSELoss()(GramMatrix()(x), target)


class NeuralAlgorithmOfArtisticStyle(StyleTransferAlgorithm):
    def __init__(self, config, on_out_img_update=lambda *args: None):
        super().__init__(config)
        self.on_out_img_update = on_out_img_update

        self.vgg = load_vgg(os.path.join(NAOAS_MODEL_PATH, "vgg_conv.pth"), self.device)

        self.content_image = Image.open(self.config.content_path)
        self.style_image = Image.open(self.config.style_path)

        content = preprocess_image(self.content_image, self.config.output_size).to(self.device)
        style = preprocess_image(self.style_image, self.config.output_size).to(self.device)

        style_layers = ['r11', 'r21', 'r31', 'r41', 'r51']
        content_layers = ['r42']

        self.loss_layers = style_layers + content_layers
        self.loss_fns = [GramMSELoss()] * len(style_layers) + [nn.MSELoss()] * len(content_layers)
        self.loss_fns = [loss_fn.to(self.device) for loss_fn in self.loss_fns]

        style_weights = [self.config.style_to_content_ratio / n ** 2 for n in [64, 128, 256, 512, 512]]
        content_weights = [1.0]
        self.weights = style_weights + content_weights

        style_targets = [GramMatrix()(A).detach() for A in self.vgg(style, style_layers)]
        content_targets = [A.detach() for A in self.vgg(content, content_layers)]
        self.targets = style_targets + content_targets

        self.opt_img = Variable(content.data.clone(), requires_grad=True)
        self.optimizer = optim.LBFGS([self.opt_img])
        self.n_iter = 0

        self.out_img = postprocess_image(self.opt_img.data.detach().clone().squeeze())
        self.on_out_img_update((self.n_iter, self.out_img))

    def perform_step(self):
        self.optimizer.step(self.closure)

    def closure(self):
        self.optimizer.zero_grad()

        out = self.vgg(self.opt_img, self.loss_layers)
        layer_losses = [self.weights[a] * self.loss_fns[a](A, self.targets[a]) for a, A in enumerate(out)]
        loss = sum(layer_losses)
        loss.backward()

        self.n_iter += 1
        logging.info("{} - Iteration: {}, loss: {}".format(self.__class__.__name__, self.n_iter, loss.item()))

        self.out_img = postprocess_image(self.opt_img.data.detach().clone().squeeze())
        if self.config.luminance_only:
            output_ycbcr = transforms.Compose([
                transforms.Lambda(lambda rgb: rgb.convert("YCbCr")),
                transforms.ToTensor()
            ])(self.out_img)

            content_ycbcr = transforms.Compose([
                transforms.Lambda(lambda rgb: rgb.convert("YCbCr")),
                transforms.ToTensor(),
                transforms.Resize(self.config.output_size)
            ])(self.content_image)

            content_ycbcr[0] = output_ycbcr[0]

            self.out_img = transforms.Compose([
                transforms.ToPILImage("YCbCr"),
                transforms.Lambda(lambda ycbcr: ycbcr.convert("RGB"))
            ])(content_ycbcr)
        self.on_out_img_update((self.n_iter, self.out_img))

        return loss


def preprocess_image(image, size):
    return transforms.Compose([
        transforms.Resize(size),
        transforms.ToTensor(),
        imagenet_normalize,
        transforms.Lambda(lambda x: x[torch.LongTensor([2, 1, 0])]),
        transforms.Lambda(lambda x: x.mul_(255))
    ])(image).unsqueeze(0)


def postprocess_image(image):
    return transforms.Compose([
        transforms.Lambda(lambda x: x.mul_(1. / 255)),
        transforms.Lambda(lambda x: x[torch.LongTensor([2, 1, 0])]),
        imagenet_denormalize,
        transforms.Lambda(lambda x: x.clamp_(0.0, 1.0)),
        transforms.ToPILImage()
    ])(image)
