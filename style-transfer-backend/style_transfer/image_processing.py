import torchvision
from PIL import Image
from torchvision import transforms

IMAGENET_MEAN = (0.48501961, 0.45795686, 0.40760392)
IMAGENET_STD = (0.229, 0.224, 0.225)
imagenet_normalize = transforms.Normalize(mean=IMAGENET_MEAN, std=IMAGENET_STD)
imagenet_denormalize = transforms.Normalize(
    mean=[-mean / std for mean, std in zip(IMAGENET_MEAN, IMAGENET_STD)],
    std=[1 / std for std in IMAGENET_STD]
)


def get_transformer(imsize=None, cropsize=None):
    transformer = []
    if imsize:
        transformer.append(transforms.Resize(imsize))
    if cropsize:
        transformer.append(transforms.RandomCrop(cropsize)),
    transformer.append(transforms.ToTensor())
    transformer.append(imagenet_normalize)
    return transforms.Compose(transformer)


def imload(path, imsize=None, cropsize=None):
    transformer = get_transformer(imsize, cropsize)
    return transformer(Image.open(path).convert("RGB")).unsqueeze(0)


def imsave(tensor, path):
    if tensor.is_cuda:
        tensor = tensor.cpu()
    tensor = torchvision.utils.make_grid(tensor)
    torchvision.utils.save_image(imagenet_denormalize(tensor).clamp_(0.0, 1.0), path)
