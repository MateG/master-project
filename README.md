# Style Transfer

This was done as part of the "Project" course of the Master programme at FER.

Not to be confused with the "Master thesis" project which can be found [here](https://gitlab.com/MateG/master-thesis).

## Setup

```Shell
cd style-transfer
./start.sh
open http://localhost:3000
```

## Technology

ML:
- PyTorch

Backend:
- Python
- Flask

Frontend:
- React
- Material UI

## Screenshots

![Implemented algorithms](/screenshots/algorithms.png)*Implemented algorithms*
![Example run](/screenshots/gatys-run.png)*Example run with output image being updated at each step*
