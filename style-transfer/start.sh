#!/bin/bash

FRONTEND_PATH="../style-transfer-frontend/"
BACKEND_PATH="../style-transfer-backend/"

start_path=$(pwd)

cd $BACKEND_PATH
echo "Starting backend server in $(pwd)"
./venv/bin/python main.py 2>stderr.log 1>stdout.log &
backend_pid=$!
echo "Backend server started with PID: $backend_pid"

cd $start_path

cd $FRONTEND_PATH
echo "Starting frontend server in $(pwd)"
BROWSER=none npm run start 2>stderr.log 1> stdout.log &
frontend_pid=$!
echo "Frontend server started with PID: $frontend_pid"

running=true

function clean_up() {
    echo ""
    echo "Backend server shutting down..."
    kill -9 $backend_pid
    echo "Frontend server shutting down..."
    kill -9 $frontend_pid
    running=false
}

trap clean_up INT
printf "(Press CTRL+C to quit)"

while $running
do
    sleep 1
done
